#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pysimplesoap.server import SoapDispatcher, SOAPHandler
from BaseHTTPServer import HTTPServer
PORT=8008

def adder(a,b):
    "Add two values"
    return a+b

dispatcher = SoapDispatcher(
    'my_dispatcher',
    location = "http://localhost:%s"%8008,
    action = 'http://localhost:%s'%8008, # SOAPAction
    namespace = "http://example.com/sample.wsdl", prefix="ns0",
    trace = True,
    ns = True)

# register the user function
dispatcher.register_function('Adder', adder,
    returns={'AddResult': int},
    args={'a': int,'b': int})

print "Starting serveron Port-%s..."%PORT
httpd = HTTPServer(("", PORT), SOAPHandler)
httpd.dispatcher = dispatcher
httpd.serve_forever()