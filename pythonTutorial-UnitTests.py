import unittest

class Foo:
    def bar(self):
        return 543

class SimpleTestCase(unittest.TestCase):

    def setUp(self):
        """Call before every test case."""
        self.foo = Foo()

    def tearDown(self):
        """Call after every test case."""
        pass

    def testA(self):
        """Test case A. note that all test method names must begin with 'test.'"""
        f = Foo()
        assert f.bar() == 543, "bar() not calculating values correctly"


if __name__ == '__main__':
    unittest.main()