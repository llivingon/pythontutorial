import ast
from flask import Flask
from flask import Flask, render_template, send_from_directory
from flaskext.mysql import MySQL
mysql = MySQL()

app = Flask(__name__)
#app.debug = True
app.config['MYSQL_DATABASE_USER'] = "pyatfuser"
app.config['MYSQL_DATABASE_PASSWORD'] = "Pa$$word"
app.config['MYSQL_DATABASE_DB'] = "PyATF"
app.config['MYSQL_DATABASE_HOST'] = "uivrdb-wc-a01q.sys.comcast.net"
mysql.init_app(app)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/user/<name>')
def user(name):
 return '<h1>Hello, %s!</h1>' % name

@app.route('/agent')
def agent():
 user_agent = request.headers.get('User-Agent')
 return '<p>Your browser is %s</p>' %user_agent


@app.route('/logs/<path:path>')
def send_js(path):
    return send_from_directory('logs', path)

@app.route("/Home/")
def Home():
    return render_template("home.html")


@app.route("/Tests/")
def Tests():
    testData = []
    i = 0
    cursor = mysql.connect().cursor()
    cursor.execute("select TestCaseId, ServiceName, TestMethodName, Comments, Environment, TestType, `Input Params` from TestCases ORDER BY TestCaseId")
    results = cursor.fetchall()
    totalTests = len(results)
    print totalTests
    while i <totalTests:
            individualTestData = []
            individualTestData.append(results[i][0]) #TestCaseId
            individualTestData.append(results[i][1].decode('unicode_escape').encode('ascii','ignore')) #ServiceName
            individualTestData.append(results[i][2].decode('unicode_escape').encode('ascii','ignore')) #TestMethodName
            individualTestData.append(results[i][3].decode('unicode_escape').encode('ascii','ignore')) #TestSummary
            individualTestData.append(results[i][4].decode('unicode_escape').encode('ascii','ignore').replace('PROD_OVERRIDE','QA/Production').replace('INT_OVERRIDE','Integration').replace('DATAWAR_OVERRIDE','Datawar')) #Environment
            individualTestData.append(results[i][5].decode('unicode_escape').encode('ascii','ignore')) #TestType
            argumentsList = results[i][6] #Input Params
            argumentsList = ast.literal_eval(argumentsList)
            argumentsList = [n.strip() for n in argumentsList]
            individualTestData.append(argumentsList)
            testData.append(individualTestData)
            i = i+1
    return render_template("tests.html", table = testData)

if __name__ == '__main__':
    app.run()