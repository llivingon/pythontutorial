def example():
    print('this code will run')
    z = 3 + 9
    print(z)
example()

def simple_addition(num1,num2):
    answer = num1 + num2
    print('num1 is', num1)
    print('num2 is', num2)
    print(answer)
simple_addition(5,3)
simple_addition(num2=3,num1=5)
simple_addition(3,5,6)


#
#
def simple(num1, num2=5):
    print('num2 is', num2)
simple(1)
simple(1,2)
#
#
x=5
def example3():
#     what we do here is defined x as a global variable.
    global x
##     now we can:
    print(x)
    x+=5
    print(x)
#
def example4():
#     what we do here is defined x as a global variable.
##     now we can:
    print(x)
    return 2


print example4()
example4()