#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import requests
import time
import xml.dom.minidom

HEADER = {'content-type': 'text/xml','charset' :'utf-8', 'Cookie': 'JSESSIONID=%sPyUnit'%str(time.strftime('%Y%m%d%H%M'))}

class SimpleTestCase(unittest.TestCase):

    def setUp(self):
        """Call before every test case."""
        self.wsdl = "http://www.webservicex.com/globalweather.asmx"
        print("Endpoint: %s"%self.wsdl)

    def tearDown(self):
        """Call after every test case."""
        print ("Request" + self.soapRequest)
        print ("Response" + str(xml.dom.minidom.parseString(self.response.content).toprettyxml()))

    def testgetCitiesByCountry(self):
        """Testing westEivrClearCustomerCache"""
        trackingId = "QA_"+self._testMethodName[4:]+str(time.time()).replace(".","")[8:]
        self.soapRequest = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://www.webserviceX.NET">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <web:GetCitiesByCountry>
				 <!--Optional:-->
				 <web:CountryName>India</web:CountryName>
			  </web:GetCitiesByCountry>
		   </soapenv:Body>
		</soapenv:Envelope>"""
        self.response =requests.post(self.wsdl, data=self.soapRequest)
        assert "<GetCitiesByCountryResult>" in self.response.content

if __name__ == '__main__':
    unittest.main()