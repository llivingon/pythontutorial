#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer

PORT_NUMBER = 9192

soapResponseMessage='''<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
   <soap:Body>
      <GetWeatherResponse xmlns="http://www.webserviceX.NET">
         <GetWeatherResult>72C</GetWeatherResult>
      </GetWeatherResponse>
   </soap:Body>
</soap:Envelope>'''

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        print "recieved do_GET request"
        import time
        time.sleep(5)
        try:
            print self.rfile.read(int(self.headers.getheader('Content-Length')))
            print self.headers.getheader('cookie')
        except:
            print "No Input Data"
        self.send_response(200)
        self.send_header('Content-type','text/xml')
        self.end_headers()
        self.wfile.write(soapResponseMessage)
        return

    def do_POST(self):
        print "recieved do_POST request"
        import time
        time.sleep(5)
        print self.headers.getheader('Host')
        print self.path
        self.send_response(200)
        self.send_header('Content-type','text/xml')
        self.end_headers()
        self.wfile.write(soapResponseMessage)
        return

try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
